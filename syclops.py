# -*- coding: utf-8 -*-
#
#	Copyright 2013 The Binary Workshop.
#	
__doc__ = """\
syclops.py - A plugin to integrate GateOne with Syclops application.

Hooks
-----
This Python plugin file implements the following hooks::

	hooks = {		
   	'WebSocket':{
      	'terminal:syclops_session_sync':session_sync,
      	'terminal:syclops_identity_get':identity_get,
      	'terminal:syclops_terminal_exit':terminal_exit,
      	'terminal:syclops_session_playback':session_playback,
      	'terminal:syclops_session_destroy':session_destroy
   	}
	}

Docstrings
----------
"""
# Meta information about this python plugin.
__version__ = '1.1'
__version_info__ = (1,1)
__license__ = "AGPLv3"
__author__ = 'John Sundarraj <sundarraj@thebw.in> Chandrasekaran <chandrasekaran@thebw.in>'

# Importing python modules.
import os,logging,glob
import pickle
import tornado.web
from gateone.core.server import BaseHandler
from gateone.core.configuration import get_settings
from gateone.core.locale import get_translation
from gateone.applications.terminal.logviewer import get_log_metadata
from gateone.applications.terminal.logviewer import render_html_playback
from gateone.applications.terminal.logviewer import render_log_frames
from tornado.escape import json_encode, json_decode
from tornado.options import options

# Get Translation.
#_ = get_translation()

# Path to our plugin directory.
PLUGIN_PATH = os.path.split(__file__)[0]

def get_256_colors(self):
    """
    Returns the rendered 256-color CSS.
    """
    colors_256_path = self.render_256_colors()
    mtime = os.stat(colors_256_path).st_mtime
    cached_filename = "%s:%s" % (colors_256_path.replace('/', '_'), mtime)
    cache_dir = self.ws.settings['cache_dir']
    cached_file_path = os.path.join(cache_dir, cached_filename)
    if os.path.exists(cached_file_path):
        with open(cached_file_path) as f:
            colors_256 = f.read()
    else:
        # Debug mode is enabled
        with open(os.path.join(cache_dir, '256_colors.css')) as f:
            colors_256 = f.read()
    return colors_256


def session_sync(self,vars):
	"""
	When *terminal:new_terminal* event occurs, :func:`GateOne.Syclops._sessionSync` gets executed
	on the client end, which triggers 'terminal:syclops_session_sync' hook	via websocket.	
	This function triggers :func:`GateOne.Syclops.sessionSync` on client end via websocket by passing 
	*params* as arguments.
	"""
	try:
		import signal
		from gateone.core.utils import get_process_tree
		session = self.ws.session
		self.ws.locations[self.ws.location]['terminal'] = {}
		stale_process = []
		for f in os.listdir('/proc'):
			pid_dir = os.path.join('/proc',f)
			if os.path.isdir(pid_dir):
				try:
					pid = int(f)
				except ValueError:
					continue
				try:
					with open(os.path.join(pid_dir,'cmdline')) as f:
						cmdline = f.read()
					if cmdline and session in cmdline:
						if 'ssh:' in cmdline:						
							stale_process.append(pid)
				except Exception as error:
					pass	
		for pid in stale_process:
			kill_process = get_process_tree(pid)
			for _pid in kill_process:
				_pid = int(_pid)
				try:
					os.kill(_pid,signal.SIGTERM)			
				except OSError:
					pass
		params = {
			'user':self.current_user['upn'],
			'session':session,			
			'status':'success'
		}
	except Exception as error:
		params = {'status':'failure'}
	data = {'terminal:syclopsjs_session_sync':params}
	self.write_message(data)

def identity_get(self,vars):
	"""
	:func:`GateOne.Syclops._sessionSync` triggers 'terminal:syclops_identity_get' hook via
	websocket, which in turn get the identity data from Syclops frontend. This function is
	the last step of initialization process, which triggers :func:`GateOne.Syclops.identityGet`
	on client end via websocket by passing *params* as arguments.
	"""
	try:
		import urllib2,json
		user = self.current_user['upn']
		ssh_dir = os.path.join(os.path.join(self.settings['user_dir'],user),'.ssh')
		request = urllib2.Request(vars['url'])
		request.add_header('Accept','application/json')
		output = urllib2.urlopen(request)
		output = json.loads(output.read())
		key = output['private_key']
		key = key.replace('\r\n','\n')
		key_path = os.path.join(ssh_dir,vars['file'])
		if not os.path.exists(key_path):
			with open(key_path,'w') as f:
				f.write(key)
			os.chmod(key_path,0600)
		else:
			pass
		params = {
			'user':vars['user'],
			'key':vars['file'],
			'ssh_dir':ssh_dir,	
			'status':'success'
		}
	except Exception as error:
		params = {'status':'failure'}
	data = {'terminal:syclopsjs_identity_get':params}
	self.write_message(data)

def _create_playback_template(self, vars):
	try:
		user = self.current_user['upn']
		log_path = os.path.join(os.path.join(self.settings['user_dir'],user),'logs')
		new_file = max(glob.iglob(log_path+'/*.golog'), key=os.path.getctime)
		playback_path = os.path.join(vars['path'],vars['base_name']+'_'+vars['id']+'.html')
		template_path = os.path.join(PLUGIN_PATH,'templates')
		template_path = os.path.join(template_path,'syclops-playback.html')
		playback_meta_details = {
			"id": vars['id'],
			"notify_url": vars['notify_url'],
			"user" : user,
			"session_id":self.ws.session,
 			"log_path":new_file,
			"storage_path":playback_path
			#
			#	The below details used for previous use case
			#
			#"template_path": template_path,
			#"playback_meta" : { #These data are for feature use
			#	"container":vars['container'],
			#	"prefix":vars['prefix'],
			#	"theme_styles":vars['theme_styles'],
			#	"colors":vars['color_styles'],
			#	"colors_256": get_256_colors(self)
			#}
		}
		name = user+"_"+self.ws.session
		with open("/opt/gateone/queue/in/"+name + '.syclops', 'wb') as f:
			pickle.dump(playback_meta_details, f, pickle.HIGHEST_PROTOCOL)
	except Exception:
		pass

def terminal_exit(self,vars):
	"""
	Closing live terminal triggers *terminal:term_ended* hook via websocket, which executes
	:func:`GateOne.Syclops._terminalExit`. This in turn triggers 'terminal:syclops_terminal_exit'
	hook via websocket, which triggers :func:`GateOne.Syclops.terminalExit` on client end via 
	websocket by passing *params* as arguments.
	"""
	try:
		user = self.current_user['upn']
		ssh_dir = os.path.join(os.path.join(self.settings['user_dir'],user),'.ssh')
		key = os.path.join(ssh_dir,vars['file'])
		known_hosts = os.path.join(ssh_dir,'known_hosts')
		if os.path.exists(key):
			os.remove(key)
		if os.path.exists(known_hosts):
			os.remove(known_hosts)		
		params = {
			'user':user,
			'key':vars['file'],
			'session':self.ws.session,	
			'status':'success'
		}		
	except Exception as error:
		params = {'status':'failure'}
	data = {'terminal:syclopsjs_terminal_exit':params}
	_create_playback_template(self, vars)
	self.write_message(data)

def session_playback(self,vars):
	"""
	:func:`GateOne.Syclops.terminalExit` executes :func:`GateOne.Syclops.sessionPlayback` on
	client end which in turn triggers 'terminal:syclops_session_playback' hook via websocket
	to execute this function and sends the playback to Syclops frontend.
	"""
	from gateone.core.log import go_logger
	syclops_log = go_logger("gateone.terminal.syclops", plugin='syclops')
	try:
		import urllib2,json,tornado.template	
		params = {
			'id':vars['id'],
			'status':'success'
		}
	except Exception as error:
		params = {
			'id':vars['id'],
			'status':'failure'
		}
	#_create_playback_template(self, vars)
	request = urllib2.Request(vars['url'])
	request.add_header('Content-Type','application/json;charset=UTF-8')
	request.add_header('Accept','application/json;charset=UTF-8')
	request.add_data(json.dumps(params))
	urllib2.urlopen(request)
	#request.close()

def session_destroy(self,vars):
	"""
	:func:`GateOne.Syclops.terminalExit` triggers 'terminal:syclops_session_destroy' via 
	websocket, which in turn executes this function. This function triggers 
	:func:`GateOne.Syclops.sessionDestroy` on client end via websocket by passing *params* 
	as arguments.
	"""
	try:
		import shutil
		from gateone.applications.terminal.app_terminal import kill_session
		kill_session(vars['session'],True)
		user = self.current_user['upn']
		user_session_dir = os.path.join(self.ws.settings['session_dir'],vars['session'])
		shutil.rmtree(user_session_dir)
		user_session_file = os.path.join(os.path.join(self.settings['user_dir'],user),'session')
		os.remove(user_session_file)
		params = {'status':'success'}
	except Exception as error:
		params = {'status':'failure'}
	data = {'terminal:syclopsjs_session_destroy':params}
	self.write_message(data)

def authenticated(func):
	def _api_validation(*args, **kwargs):
		import hashlib
		import io
		from datetime import datetime
		from gateone.core.utils import create_signature, convert_to_timedelta
		from gateone.core.log import go_logger
		self = args[0]
		auth_log = go_logger('gateone.auth')
		global_settings = get_settings(options.settings_dir)
		settings = global_settings["*"]["gateone"]
		auth_obj = {
			"api_key" : self.get_argument("api_key"),
			"upn" : self.get_argument("upn"),
			"timestamp" : self.get_argument("timestamp"),
			"signature_method" :self.get_argument("signature_method"),
			"api_version":self.get_argument("api_version"),
			"signature" :self.get_argument("signature")
		}
		api_key = auth_obj.get('api_key', None)
		if not api_key:
			auth_log.error(_(
				'API AUTH: Invalid API authentication object (missing api_key).'
			))
			return None
		upn = auth_obj['upn']
		timestamp = str(auth_obj['timestamp']) # str in case integer
		signature = auth_obj['signature']
		signature_method = auth_obj['signature_method']
		api_version = auth_obj['api_version']
		supported_hmacs = {
			'HMAC-SHA1': hashlib.sha1,
			'HMAC-SHA256': hashlib.sha256,
			'HMAC-SHA384': hashlib.sha384,
			'HMAC-SHA512': hashlib.sha512,
		}
		if signature_method not in supported_hmacs:
			auth_log.error(_(
					'API AUTH: Unsupported API auth '
					'signature method: %s' % signature_method))
			return None
		hmac_algo = supported_hmacs[signature_method]
		if api_version != "1.0":
			auth_log.error(_(
					'API AUTH: Unsupported API version:'
					'%s' % api_version))
			return None
		try:
			secret = settings['api_keys'][api_key]
		except KeyError:
			auth_log.error(_(
				'API AUTH: API Key not found.'))
			return None
		sig_check = create_signature(
			secret, api_key, upn, timestamp, hmac_algo=hmac_algo)
		if sig_check != signature:
			auth_log.error(_('API AUTH: Signature check failed.'))
			return None
		#window = settings['api_timestamp_window']
		window = convert_to_timedelta(settings['api_timestamp_window'])
		then = datetime.fromtimestamp(int(timestamp)/1000)
		time_diff = datetime.now() - then
		if time_diff > window:
			auth_log.error(_(
				"API AUTH: Authentication failed due to an expired auth "
				"object.  If you just restarted the server this is "
				"normal (users just need to reload the page).  If "
				" this problem persists it could be a problem with "
				"the server's clock (either this server or the "
				"server(s) embedding Gate One)."
			))
			return None
		auth_log.info(_("API Authentication Successful"))
		return func(*args, **kwargs)
	return _api_validation

class SyclopsForceCloseHandler(BaseHandler):

	@authenticated
	def get(self):
		pass

	@authenticated
	def post(self):
		settings = get_settings(options.settings_dir)
		user = self.get_argument("upn")
		try:
			import shutil, signal, time
			from gateone.applications.terminal.app_terminal import kill_session
			from gateone.core.utils import get_process_tree

			#kill force closed session's ssh connection
			session = self.get_argument("session_id")
			stale_process = []
			for f in os.listdir('/proc'):
      				pid_dir = os.path.join('/proc',f)
      				if os.path.isdir(pid_dir):
       					try:
          					pid = int(f)
        				except ValueError:
          					continue
        				try:
          					with open(os.path.join(pid_dir,'cmdline')) as f:
            						cmdline = f.read()
          						if cmdline and session in cmdline:
            							if 'ssh:' in cmdline:
              								stale_process.append(pid)
					except Exception as error:
          					pass
			for pid in stale_process:
			      	kill_process = get_process_tree(pid)
      				for _pid in kill_process:
        				_pid = int(_pid)
        				try:
          					os.kill(_pid,signal.SIGTERM)
        				except OSError:
          					pass

			#clear user session's details
			kill_session(self.get_argument("session_id"),True)
			user_session_dir = os.path.join(settings["*"]["gateone"]['session_dir'],self.get_argument("session_id"))
			shutil.rmtree(user_session_dir)
			user_session_file = os.path.join(os.path.join(settings["*"]["gateone"]['user_dir'],user),'session')
			os.remove(user_session_file)
			#get log file and generate playback out of it.
			log_path = os.path.join(os.path.join(settings["*"]["gateone"]["user_dir"],user),'logs')
			new_file = max(glob.iglob(log_path+'/*.golog'), key=os.path.getctime)
			playback_meta_details = {
				"id": self.get_argument("id"),
				"notify_url":self.get_argument("notify_url"),
				"user" :user,
				"session_id":self.get_argument("session_id"),
				"log_path":new_file,
				"storage_path": self.get_argument("storage_path")
			}
			name = user+"_"+self.get_argument("session_id")
			with open("/opt/gateone/queue/in/"+name+'.syclops', 'wb') as f:
				pickle.dump(playback_meta_details, f, pickle.HIGHEST_PROTOCOL)
		except Exception:
			pass

hooks = {
	'Web': [(r"/syclops/forceclose", SyclopsForceCloseHandler)],
	'WebSocket':{
		'terminal:syclops_session_sync':session_sync,
		'terminal:syclops_identity_get':identity_get,
		'terminal:syclops_terminal_exit':terminal_exit,
		'terminal:syclops_session_playback':session_playback,
		'terminal:syclops_session_destroy':session_destroy
	}
}
