/**
 * @file
 * Plugin's main js file.
 */
GateOne.Base.superSandbox("GateOne.Syclops", ["GateOne.Terminal"], function(window, undefined) {
"use strict";
	var document = window.document;	// Traditonal way for defining document.
	// Changing naming conventions.
	var go = GateOne,
		urlObj = (window.URL || window.webkitURL),
		logFatal = GateOne.Logging.logFatal,
		logError = GateOne.Logging.logError,
		logWarning = GateOne.Logging.logWarning,
		logInfo = GateOne.Logging.logInfo,
		logDebug = GateOne.Logging.logDebug;	
	go.Base.module(GateOne,'Syclops','1.1',['Base']);	// Telling GateOne object to include Syclops as object, which results in GateOne.Syclops object.
	go.Syclops.settings = {};	// Scafold object (not really necessary).
	go.noSavePrefs['syclopsSettings'] = null;// This ensures that this plugin settings doesn't get saved.
	go.Base.update(go.Syclops,{	// Internal properties of GateOne.Syclops object.		
		init:function() {	// Initializer.
			go.Syclops.settings = go.prefs.syclopsSettings;	// Set this plugin's settings which can be passed in via GateOne.init().
			go.prefs.syclopsSettings = null; // Extra precaution to avoid plugin's settings being saved in GateOne.prefs.
			// Adding websocket actions.
			go.Net.addAction('terminal:syclopsjs_session_sync',go.Syclops.sessionSync);
			go.Net.addAction('terminal:syclopsjs_identity_get',go.Syclops.identityGet);
			go.Net.addAction('terminal:syclopsjs_terminal_exit',go.Syclops.terminalExit);
			go.Net.addAction('terminal:term_ended',go.Syclops._terminalExit);
			go.Net.addAction('terminal:syclopsjs_session_destroy',go.Syclops.sessionDestroy);
			// Adding event handlers.
			go.Events.on('terminal:new_terminal',go.Syclops._sessionSync);
		},
		_sessionSync:function() {	// Wrapper for synchronizing session.
			go.prefs.embedded = true;	// Switching GateOne to run on embedded mode, to avoid creation of new terminal, when closing existing one.
			go.ws.send(JSON.stringify({'terminal:syclops_session_sync':true}));	// Sending hook's via websocket.
		},		
		sessionSync:function(params) {	// Session synchronization.
			if(params.status=='success') {	// Checking status from server.
				go.Playback.hideControls(); // Hiding playback controls.	
				var url = go.Syclops.settings.session.syncURL;	// Session sync url.				
				var postParams = {	// Constructing object.
					'user':params.user,	// User name.
					'session':params.session	//Session name.
				};				
				var response = null;	// Setting response variable.				
				var http = new XMLHttpRequest();	// Instantiating XMLHttpRequest's object.
				http.open('POST',url,true); // Opening request.
				// Setting request headers.
				http.setRequestHeader('Content-Type','application/json;charset=UTF-8');
				http.setRequestHeader('Accept','application/json;charset=UTF-8');
				http.onreadystatechange = function() {	// On ready state.				
					if((http.readyState==4)&&(http.status==200)) {	// Checking request conditions.						
						response = JSON.parse(http.responseText);	// Obtaining response.
					}
				}				
				http.send(JSON.stringify(postParams));	// Posting params.
				go.Visual.displayMessage('<i>Syclops: Connecting to the server to sync a session</i>');
				setTimeout(function() {	// Using timeout, becoz of some time delay problems.				
					if(response.status=='success') {	// Checking response status.					
						go.Syclops.settings.session.id = response.id;	// Setting session id.						
						go.Visual.displayMessage('<i>Syclops:Session synchronized successfully</i>');	// Displaying message.
						var vars = {	// Constructing params.
							'user':params.user, // User name.
							'file':go.Syclops.settings.identity.fileName, // Identity file name.
							'url':go.Syclops.settings.identity.getURL // Url to get identity data.
						};								    				
         			go.ws.send(JSON.stringify({'terminal:syclops_identity_get':vars}));	// Sending hook's via websocket.
            		window.parent.onbeforeunload = function(event) { // Registering callback for parent window.onbeforeunload event.
               		for(var terminal in go.Terminal.terminals) { // Iterating through all terminals.
                 			if(terminal%1===0) { // Checking for an integer.
                     		try {
                        		go.Syclops.sessionPlayback(params.session,terminal); // Capturing session playback.
                     		} catch(error) {
                        		logError(error);  // Logging error.
                     		}
                  		}
               		}
							var url = go.Syclops.settings.session.waitURL; // Session wait url for sending current playback to the server.
            			var postParams = {'wait':'1'};	// Constructing object.
            			var http = new XMLHttpRequest(); // Instantiating XMLHttpRequest's object.						
            			http.open('POST',url,false); // Opening request.
            			// Setting request headers.
            			http.setRequestHeader('Content-Type','application/json;charset=UTF-8');
            			http.setRequestHeader('Accept','application/json;charset=UTF-8');
            			http.send(JSON.stringify(postParams)); // Posting params.							
               		return 'WARNING: You are currently running an active session'; // Returning custom message.
            		}						
					}					
					if(response.status=='failure') {	// Checking response status.               	
               	go.Visual.displayMessage('<i>Syclops:Unable to synchronize session</i>');	// Displaying message.
					}
				},3000);				
			}
			if(params.status=='failure') { // Checking status from server.				
				go.Visual.displayMessage('<i>Syclops:Unable to synchronize session</i>');	// Displaying message.
			}
		},	

		identityGet:function(params) {	// Get identity file.	
			if(params.status=='success') {	// Checking status from server.				
				window.parent.jQuery.Syclops.startElapedTimer();
				go.Visual.displayMessage('<i>Syclops:Updated identity</i>');	// Displaying message.
				var autoConnectURL = go.Syclops.settings.ssh.autoConnectURL+'/?identities='+params.key;	// Setting the ssh url with query string.
				try {				
					go.Terminal.sendString(autoConnectURL+'\n');	// Connecting to the machine.
				} catch(error) { 					
					logError(error); // Logging error.
					go.Visual.displayMessage('<i>Syclops:Could not auto connect to machine</i>');  // Displaying message.
				}
				var origin;
				if(!location.origin) {
				  origin = window.location.protocol +"//"+window.location.hostname+ (window.location.port? ':'+ window.location.port:'');
				} else {
				  origin = location.origin;
        }
				  go.Syclops.settings.session.exitURL = origin+'/session/'+go.Syclops.settings.session.id+'/close'; // Session close url.
				
			}			
			if(params.status=='failure') {	// Checking status from server.			
				go.Visual.displayMessage('<i>Syclops:Unable to get the identity</i>');	// Displaying message.
			}
		},		
		_terminalExit:function() {	// Wrapper for exiting terminal.
			var vars = { // Constructing object.
				'id':go.Syclops.settings.session.id,
				'base_name':go.Syclops.settings.session.baseName,
				'notify_url':go.Syclops.settings.session.notifyURL,
				'file':go.Syclops.settings.identity.fileName,	// Identity filename.
				'path':go.Syclops.settings.playback.storagePath,
				'prefix':go.prefs.prefix,  // DOM element prefix.
        'container':go.prefs.goDiv.split('#')[1], // DOM Container.
        'theme_styles':go.Utils.getNode('#'+go.prefs.prefix+'theme').innerHTML, // Theme CSS that are loaded.
        'color_styles':go.Utils.getNode('#'+go.prefs.prefix+'text_colors').innerHTML  // Color CSS that are loaded.
			};        
         go.ws.send(JSON.stringify({'terminal:syclops_terminal_exit':vars}));	// Sending hook's via websocket.
		},	

		forceClose : function() {
			go.Syclops._terminalExit();
		},

		sessionPlayback:function(session,terminal) {			
			/*var terminalRecording = JSON.stringify(go.Terminal.terminals[terminal]['playbackFrames']);*/ // Exact recorded frames.
			var vars = { // Constructing object.
				'id':go.Syclops.settings.session.id, // Session node id.
				'session':session, // Session name.
				'url':go.Syclops.settings.session.playbackURL, // Url to which the session playback has to sent.
				'path':go.Syclops.settings.playback.storagePath, // System real path where the playback file gets stored.
				/*'recording': terminalRecording,*/   // Recorded frames, basically objects.
				'prefix':go.prefs.prefix,  // DOM element prefix.
				'container':go.prefs.goDiv.split('#')[1], // DOM Container.
				'theme_styles':go.Utils.getNode('#'+go.prefs.prefix+'theme').innerHTML, // Theme CSS that are loaded.
				'color_styles':go.Utils.getNode('#'+go.prefs.prefix+'text_colors').innerHTML  // Color CSS that are loaded.
			};
			go.ws.send(JSON.stringify({'terminal:syclops_session_playback':vars}));   // Sending hook's via websocket.			
		},
		terminalExit:function(params) {	// Exiting terminal.			
			if(params.status=='success') {	// Checking status from server.
				try {
					go.Visual.displayMessage('<i>Syclops:Identity removed successfully</i>');	// Displaying message.					
					for(var terminal in go.Terminal.terminals) {	// Iterating through all terminals(multiple terminals are possible for a single session).	
						if(terminal%1===0) { // Checking for an integer.
							try {								
								go.Syclops.sessionPlayback(params.session,terminal);
							} catch(error) {						
								go.Visual.displayMessage('<i>Syclops:Got some strange error</i>');	// Displaying message.
								logError(error);	// Logging error.
							}
							go.Terminal.closeTerminal(terminal,false,'<i>Syclops:Exiting terminal</i>');	// Closing terminal.
						}					
         		}
					var vars = { // Constructing object.
						'session':params.session  // Session name.
					};  	
					go.ws.send(JSON.stringify({'terminal:syclops_session_destroy':vars}));	// Sending hook's via websocket.
				} catch(error) {				
					go.Visual.displayMessage('<i>Syclops:Got some strange error</i>'); // Displaying message.
					logError(error);	// Logging error.
				}
			}			
			if(params.status=='failure') {	// Checking status from server.
				go.Visual.displayMessage('<i>Syclops:Unable to exit terminal</i>');	// Displaying message.
			}
		},		
		sessionDestroy:function(params) {	// Destroying session.
			if(params.status=='success') {	// Checking status from server.
				go.Visual.displayMessage('<i>Syclops:Session destroyed</i>'); // Displaying message.
				window.parent.onbeforeunload = null;	// Setting null value for parent window.onbeforeunload event.
				setTimeout(function() {	// Using timeout, becoz of some time delay problems.
					var exitURL = go.Syclops.settings.session.exitURL;
					localStorage.removeItem("go_default_prefs");
					window.parent.location.href = exitURL;	// Redirecting parent window location after session exit.
				},100);
			}	
			if(params.status=='failure') { // Checking status from server.
				go.Visual.displayMessage('<i>Syclops:Unable to destroy session</i>'); // Displaying message.
			}
		}
	});
});
